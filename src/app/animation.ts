import { trigger, animate, transition, style, query } from '@angular/animations';

export const routeStateTrigger = trigger('routeState', [
    transition('* => *', [
        query(':enter', [
            style({ position: 'absolute', opacity: 0 })
        ], { optional: true }),
        query(':leave', [
            animate(300, style({ opacity: 0 }))
        ], { optional: true }),
        query(':enter', [
            style({ position: 'relative', opacity: 0 }),
            animate(300, style({ display: 'visible', opacity: 1 }))
        ], { optional: true })
    ])
]);