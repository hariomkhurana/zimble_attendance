import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MainComponent } from "./main.component";
import { AuthGuardMain } from "../auth/auth-guard.service";

const routes: Routes = [
  {
    path: "main",
    component: MainComponent,
    //canActivate: [AuthGuardMain],
    children: [
      // {
      //   path: "dashboard",
      //   loadChildren: () =>
      //     import("./dashboard/dashboard.module").then((m) => m.DashboardModule),
      //     data: { state: 'dashboard' } 
      // },
      {
        path: "admin",
        loadChildren: () =>
          import("./admin/admin-management/admin-management.module").then((m) => m.AdminManagementModule),
          data: { state: 'admin' } 
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MainRoutingModule {}
