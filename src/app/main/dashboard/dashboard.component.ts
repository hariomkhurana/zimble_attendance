import { Component, AfterViewInit, OnInit } from '@angular/core';
import { CommonService } from '../../shared/common.service';
import { DashboardService } from './dashboard.service';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { ModalDismissReasons, NgbModal, NgbModalOptions, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { MatSnackBar } from '@angular/material';
@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
	isLoading: boolean=false;
	dummyCard=[1,2]
	dummyBlog=[1,2,3]
	Name:any;
	MessageList:any;
	NewMatches:[];
	bloglist:any;
	userdetail:any;
	userData:any;
	items = [1];
	totalFav:number = 0;
	modelOptions: NgbModalOptions = {
		backdrop: "static",
		keyboard: false,
		size:'md',
		centered:true
	  };
	  closeResult: string;
	  private modalRef: NgbModalRef;
	messageText = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.1500s,Lorem Ipsum is simply dummy text of the printing and typesetting industry.1500s,Lorem Ipsum is simply dummy text of the printing and typesetting industry.1500sLorem Ipsum is simply dummy text of the printing and typesetting industry.1500s.";
  
	constructor( 
	private DashboardService:DashboardService,
	private CommonService:CommonService,
	private router: Router,
	private modalService: NgbModal,
	private _snackBar: MatSnackBar
			) { }

	ngOnInit() {
	  this.getDashboard();
	  this.userdetail= this.CommonService.getUser();
	  this.getBlogList();
	  this.userData =  JSON.parse(localStorage.getItem('usPdata')) || 'noemail' ;
      this.Name = this.userData['1']
	}

	newMatch: []
	newMessage: []
	ismessageImg:boolean = false;
	getDashboard() {
		this.isLoading = true;
		this.DashboardService.dashboardCount().subscribe (
		  res => {
			this.isLoading = false;
			if ((res["message"] = "Success")) {
			  this.totalFav  = res['data']['totalFavorites'];
			  this.MessageList = res['data']['newMessage'];
			  this.newMatch = res['data']['newMatch'];
			  if(res['data']['newMessage'][0]['message'].indexOf('https://') !== -1 ) {
			
				this.ismessageImg = true
			  console.log("INNEWMWSSAGE IMAGE")
			}
			  if(this.MessageList.length > 0) {
				this.MessageList.forEach(element => {
					element['humanDate'] = moment.unix(element['sentAt']).format('MMMM Do, YYYY h:mm A')
					
					if(element['receiverId'] !== this.userdetail['userId']) {
					  element['displayImage']  = element['receiverProfilePic'];
					  element['displayName']  = element['receiverFullName'];
					 
					}else {
					  element['displayImage']  = element['senderProfilePic'];
					  element['displayName']  = element['senderFullNamee'];
					}
		  
				  });

			  }

			 
			} else {
			}
			this.isLoading = false;
		  },
		  err => {
			this.isLoading = false;
		  }
		);
	  }
	  getBlogList() {
		this.isLoading = true;
		this.DashboardService.getblog(this.userdetail['userId']).subscribe (
		  res => {
			this.isLoading = false;
			if ((res["message"] = "Success")) {
		this.bloglist = res["data"]["blogList"];
		console.log('final',this.bloglist)
		this.bloglist.forEach(element => {
			element['bioLength'] = element['bio'].length; 
			element['nameLength'] = element['fullName'].length; 
			element['titleLength'] = element['title'].length; 
			element['humanDate'] = moment.unix(element['insertDate']).format('MMMM Do, YYYY')
			if(element['bioLength'] > 98) {
				element["bio"] = element["bio"].substring(0,98) + '...';
			   }
			if(element['nameLength'] > 15) {
			  element["fullName"] = element["fullName"].substring(0,10) + '...';
			}
			if(element['titleLength'] > 15) {
			  element["title"] = element["title"].substring(0,15) + '...';
			}
		  });
        console.log(this.bloglist,'-------')
			} else {
			  this.bloglist = [];
			}      },
		  err => {
			this.isLoading = false;
		  }
		);
  }
  navigateReadMore(id){
    this.router.navigate(["/main/blog/single-blog/"], { queryParams: { id: id}, preserveQueryParams: false });
  }
  navEditBlog() {
	localStorage.setItem('side0909','8');
  }
  navigateViewAll (value) {
	  if (value == 'MESSAGE') {
		localStorage.setItem('side0909','2');
		this.router.navigate(["/main/message"]);
	  }
	  if (value == 'MATCH') {
		localStorage.setItem('side0909','3');
		this.router.navigate(["/main/match"]);
	  }
	  if (value == 'BLOG') {
		localStorage.setItem('side0909','8');
		this.router.navigate(["/main/blog"]);
	  }
	  if (value == 'FAV') {
		localStorage.setItem('side0909','7');
		this.router.navigate(["/main/favourite"]);
	  }


  }
  blogId: string;
  deleteblogModal(row,content,btn) {
	this.blogId = row;
	debugger;
    btn && btn.parentElement && btn.parentElement.parentElement && btn.parentElement.parentElement.blur();
    this.modalRef = this.modalService.open(content, this.modelOptions);
    this.modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }
  RemoveBlog() {
    this.isLoading = true;
		this.DashboardService.deleteBlog(this.blogId).subscribe(
			res => {
			  this.isLoading = false;
			  if ((res["message"] = "Success")) {
          this.modalRef.close();
				this.getBlogList();
				return this._snackBar.open("Blog deleted successfully","",{
					duration: 3000,
					horizontalPosition:'right',
					verticalPosition:'top',
					panelClass: ['failure']
				  });
				
			  } else {
				
			  }      },
			err => {
			  this.isLoading = false;
			}
		  );
	}
	NavigateMessage(data) {

		let userSend = "";
		if(this.userdetail['userId'] == data['receiverId']) {
		  console.log('match ')
		  userSend = data['senderId']
		}
		else {
		 userSend = data['receiverId']
		}
			localStorage.setItem('usMessto',userSend);
			localStorage.setItem('side0909','2');
			this.router.navigate(["main/message/add-message"], { queryParams: { id: userSend, 1: '4' }, preserveQueryParams: false });
		  }

		  viewProfile(data,from) {
			//1 from search
			console.log(data);
			localStorage.setItem('side0909','9');
		   this.router.navigate(["/main/other"], { queryParams: { id: data.userId, t: from }, preserveQueryParams: false });
		  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return "by pressing ESC";
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return "by clicking on a backdrop";
    } else {
      return `with: ${reason}`;
    }
  }
  
}


