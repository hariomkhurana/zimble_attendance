import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminManagementRoutingModule } from './admin-management-routing.module';
import { AdminManagementComponent } from './admin-management/admin-management.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { AddManagerComponent } from './add-manager/add-manager.component';
import { ManagerListComponent } from './manager-list/manager-list.component';
import { AddResourceComponent } from './add-resource/add-resource.component';
import { AllResourceComponent } from './all-resource/all-resource.component';
import { AssessmentListComponent } from './assessment-list/assessment-list.component';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { CandidateListComponent } from './candidate-list/candidate-list.component';
import { DepartmentListComponent } from './department-list/department-list.component';
import { FacilityListComponent } from './facility-list/facility-list.component';


@NgModule({
  declarations: [AdminManagementComponent, AdminDashboardComponent,AddManagerComponent, ManagerListComponent,AddResourceComponent,AllResourceComponent,AssessmentListComponent,EmployeeListComponent,CandidateListComponent,DepartmentListComponent,FacilityListComponent],
  imports: [
    CommonModule,
    AdminManagementRoutingModule
  ]
})
export class AdminManagementModule { }
