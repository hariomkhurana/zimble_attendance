import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AllResourceComponent } from './all-resource/all-resource.component';
import { AddManagerComponent } from './add-manager/add-manager.component';
import { AddResourceComponent } from './add-resource/add-resource.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { AdminManagementComponent } from './admin-management/admin-management.component';
import { ManagerListComponent } from './manager-list/manager-list.component';
import { AssessmentListComponent } from './assessment-list/assessment-list.component';
import { CandidateListComponent } from './candidate-list/candidate-list.component';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { DepartmentListComponent } from './department-list/department-list.component';
import { FacilityListComponent } from './facility-list/facility-list.component';



const routes: Routes = [
  {path:"",component: AdminManagementComponent,
  children:[
    { path: '', redirectTo: '/main/admin', pathMatch: 'full' },
    {path:"dashboard",component:AdminDashboardComponent},
    {path:"add-manager",component:AddManagerComponent},
    {path:"all-manager",component:ManagerListComponent},
    {path:"add-resource",component:AddResourceComponent},
    {path:"all-resource",component:AllResourceComponent},
    {path:"view-assessment",component:AssessmentListComponent},
    {path:"view-candidate",component:CandidateListComponent},
    {path:"view-employee",component:EmployeeListComponent},
    {path:"view-department",component:DepartmentListComponent},
    {path:"view-facility",component:FacilityListComponent},
  ]
}
];


// const routes: Routes = [
//   {path:"",component:UserManagementComponent,
// children:[
//   { path: '', redirectTo: '/main/user-management/view-user', pathMatch: 'full' },
//   {path:"view-user",component:ViewUserComponent},
//   {path:"table",component:ViewReportedComponent},
//   {path:"admin-dashboard",component:AdminDashboardComponent},
//   {path:"add-manager",component:AddUserComponent}
// ]
// }
// ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminManagementRoutingModule { }
