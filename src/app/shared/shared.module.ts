
import { MenuItems } from './menu-items/menu-items';
import { AccordionAnchorDirective, AccordionLinkDirective, AccordionDirective } from './accordion';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatFormFieldModule, MatDatepickerModule, MatInputModule, MatOptionModule, MatSelectModule, MatRadioModule, MatTableModule, MatButtonModule, MatIconModule, MatToolbarModule, MatSidenavModule, MatListModule, MatMenuModule } from '@angular/material';
import { MatTabsModule } from "@angular/material/tabs";
import { NgxDatatableModule } from "@swimlane/ngx-datatable";
import { MatNativeDateModule } from "@angular/material/core";
import { SpinnerComponent } from './spinner/spinner.component';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { SearchBtnComponent } from './search-btn/search-btn.component';
import { ResetBtnComponent } from './reset-btn/reset-btn.component';
import { AddBtnComponent } from './add-btn/add-btn.component';
import { AlertComponent } from './alert/alert.component';
import { AlertModule, AlertConfig } from 'ngx-bootstrap/alert';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { ImageCropperModule } from 'ngx-image-cropper';
import { CommonModule } from '@angular/common';
import { SkeletonScreenComponent } from './skeleton-screen/skeleton-screen.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { UiSwitchModule } from 'ngx-toggle-switch';
// import { NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';
@NgModule({
  declarations: [
    AccordionAnchorDirective,
    AccordionLinkDirective,
    AccordionDirective,
    SpinnerComponent,
    SearchBtnComponent,
    ResetBtnComponent,
    AddBtnComponent,
    AlertComponent,
    SkeletonScreenComponent

  ],
  imports:[CommonModule,  MatSnackBarModule,FormsModule,AlertModule.forRoot(),NgMultiSelectDropDownModule.forRoot()],
  exports: [
    FormsModule, ReactiveFormsModule,
    AccordionAnchorDirective,
    AccordionLinkDirective,
    AccordionDirective,
    MatFormFieldModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatInputModule,
    MatOptionModule,
    MatIconModule,
    MatSelectModule,
    MatTabsModule,
    MatToolbarModule,
    MatSidenavModule,
    MatRadioModule,
    MatMenuModule,
    MatSnackBarModule,
    MatTableModule,MatButtonModule,MatListModule,
    NgxDatatableModule,
    SpinnerComponent,
    SearchBtnComponent,
    ResetBtnComponent,
    AlertComponent,
    AlertModule,
    NgMultiSelectDropDownModule,
    ImageCropperModule,
    CommonModule,
    SkeletonScreenComponent,MatProgressSpinnerModule,
    UiSwitchModule

   ],
  providers: [ MenuItems,AlertConfig ]
})
export class SharedModule { }
