import { Injectable } from "@angular/core";


export interface Menu {
  state: string;
  name: string;
  type: string;
  iconUrl: string;
  iconUrlActive:string
}



// const MENUITEMS = [
//   { state: 'dashboard', name: 'Dashboard', iconUrl:'assets/images/dashboard_white.png', type: 'link', iconUrlActive:'assets/images/dashboard_color.png'},
//   { state: 'search', name: 'Search', iconUrl:'assets/images/search_color.png', type: 'link',iconUrlActive:'assets/images/search_white.png'},
//   { state: 'message', name: 'Message', iconUrl:'assets/images/message_white.png', type: 'link',iconUrlActive:'assets/images/message_color.png'},
//   { state: 'match', name: 'My Matches', iconUrl:'assets/images/match_white.png', type: 'link',iconUrlActive:'assets/images/match_color.png'},
//   { state: 'profile', name: 'My Profile', iconUrl:'assets/images/profile_white.png', type: 'link',iconUrlActive:'assets/images/profile_color.png'},
//   { state: 'subscription', name: 'Subscription', iconUrl:'assets/images/ic_subscription.svg', type: 'link',iconUrlActive:'assets/images/ic_Subscription_fill.svg'},
//   { state: 'payment', name: 'Payment', iconUrl:'assets/images/payment_white.png', type: 'link',iconUrlActive:'assets/images/payment_color.png'},
//   { state: 'favourite', name: 'My Favorite', iconUrl:'assets/images/fav_white.png', type: 'link',iconUrlActive:'assets/images/fav_color.png'},
//   { state: 'blog', name: 'My Blogs', iconUrl:'assets/images/writing_white.png', type: 'link',iconUrlActive:'assets/images/writing_color.png'},

//      ];

const MENUITEMS = [
  { state: 'admin/view-department', name: 'Department', iconUrl:"assets/images/Dashboard_icon.svg", type: 'link', iconUrlActive:'assets/images/Dashboard_icon_white.svg'},
  { state: 'admin/view-facility', name: 'Facility', iconUrl:"assets/images/Managers_icon.svg", type: 'link', iconUrlActive:'assets/images/Managers_icon_white.svg'},
  // { state: 'admin/all-manager', name: 'Manager', iconUrl:"assets/images/Wythe-Veterinary_logo.png", type: 'link', iconUrlActive:'assets/images/Wythe-Veterinary_logo.png'},


  // { state: 'search', name: 'Search', iconUrl:'assets/images/search_color.png', type: 'link',iconUrlActive:'assets/images/search_white.png'},
  // { state: 'message', name: 'Message', iconUrl:'assets/images/message_white.png', type: 'link',iconUrlActive:'assets/images/message_color.png'},
  // { state: 'match', name: 'My Matches', iconUrl:'assets/images/match_white.png', type: 'link',iconUrlActive:'assets/images/match_color.png'},
  // { state: 'profile', name: 'My Profile', iconUrl:'assets/images/profile_white.png', type: 'link',iconUrlActive:'assets/images/profile_color.png'},
  // { state: 'subscription', name: 'Subscription', iconUrl:'assets/images/ic_subscription.svg', type: 'link',iconUrlActive:'assets/images/ic_Subscription_fill.svg'},
  // { state: 'payment', name: 'Payment', iconUrl:'assets/images/payment_white.png', type: 'link',iconUrlActive:'assets/images/payment_color.png'},
  // { state: 'favourite', name: 'My Favorite', iconUrl:'assets/images/fav_white.png', type: 'link',iconUrlActive:'assets/images/fav_color.png'},
  // { state: 'blog', name: 'My Blogs', iconUrl:'assets/images/writing_white.png', type: 'link',iconUrlActive:'assets/images/writing_color.png'},

     ];


@Injectable()
export class MenuItems {
  
  getMenuitem(): Menu[] {
    return MENUITEMS;
  }
}
