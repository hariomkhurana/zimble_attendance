import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-skeleton-screen',
  templateUrl: './skeleton-screen.component.html',
  styleUrls: ['./skeleton-screen.component.scss']
})
export class SkeletonScreenComponent implements OnInit {
	dummyData = [1,2,3,4,5,6,7,8,9];
  constructor() { }

  ngOnInit() {
  }

}
