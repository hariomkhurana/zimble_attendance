import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit {
  dismissible = true;
  @Input() message: any;
  @Input() type: any;
  
  alertType = { type: this.type }

  constructor() { }

  ngOnInit(): void {
    console.log(this.message)
    console.log(this.type)
    console.log(this.alertType)
   }

}
