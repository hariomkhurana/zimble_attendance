import {
  EventEmitter,
  ChangeDetectorRef,
  Component,
  NgZone,
  OnDestroy,
  ViewChild,
  HostListener,
  Directive,
  AfterViewInit,
  Output,Input, OnInit
} from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { MenuItems } from '../../../shared/menu-items/menu-items';
import { CommonService } from '../../../shared/common.service';
import { StreamService } from '../../../../app/shared/stream.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class AppSidebarComponent implements OnInit, OnDestroy {
  @Output() messageEvent = new EventEmitter<string>();
  @Input() isMobileActive: any;
  mobileQuery: MediaQueryList;
  isAllow :boolean = false;
  isPodcast:boolean = false;
  userData:any;
  userP:any;
  userG:any;
  userName:any
  podcastVal:boolean = false;
  private _mobileQueryListener: () => void;

  constructor(
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    public menuItems: MenuItems,
    public common :CommonService,
    private streamService: StreamService
  ) {
    this.mobileQuery = media.matchMedia('(min-width: 768px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }
  ngOnInit() {
   
  }
  activeRoute(index) {
    console.log(index);
    localStorage.setItem("activeIndex", index);
  }

  readLocalStorageValue(key: string): number {
    return parseInt(localStorage.getItem(key));
  }
  logOut() {
    this.common.logOut();
  }
  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }
}
