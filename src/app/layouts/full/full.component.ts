
import { MediaMatcher } from '@angular/cdk/layout';
import { NavigationStart, Router } from '@angular/router';
import {
  ChangeDetectorRef,
  Component,
  NgZone,
  OnDestroy,
  ViewChild,
  HostListener,
  Directive,
  AfterViewInit,
  OnInit
} from '@angular/core';
import { MenuItems } from '../../shared/menu-items/menu-items';
import { CommonService } from '../../shared/common.service';
import {
  transition,
  trigger,
  query,
  style,
  animate,
  group,
  animateChild,
  state
} from '@angular/animations';
import { MatSnackBar } from '@angular/material';
import { AuthServiceLocal } from '../../auth/auth.service';
import { StreamService } from '../../shared/stream.service';

declare var $: any;
/** @title Responsive sidenav */
@Component({
  selector: 'app-full-layout',
  templateUrl: 'full.component.html',
  styleUrls: ['full.component.scss'],
  animations: [
    trigger('routerTransition', [
      transition('* <=> *', [    
        query(':enter, :leave', style({ position: 'fixed', opacity: 0 }),{ optional: true }),
        group([ 
          query(':enter', [
            style({ opacity:0 }),
            animate('600ms ease-in-out', style({ opacity:0 })),

          ],{ optional: true }),
          query(':leave', [
            style({ opacity:0.5 }),
            animate('600ms ease-in-out', style({ opacity:0 }))],{ optional: true }),
        ])
      ])
    ])
   ]
})
export class FullComponent implements OnDestroy, AfterViewInit,OnInit {
  mobileQuery: MediaQueryList;
  isMenuOpen = true;
  contentMargin: number ;
  message: string;
  public screenWidth: any;
  public screenHeight: any;
  isMobile: boolean = false;
  detail:any;
  Name:string;
  routeName:string;
  profilePic:string;
  headerTitle:string = "Dashboard";
  userData:any;
  showScroll : boolean = true;
  isLoading:boolean = false;
  isPodcast:boolean = false;
  NoScroll =["Search","My Matches","My Favorites"];
  currentScreenWidth:any;
  private _mobileQueryListener: () => void;

  // MENUITEMS = [
  //   { state: 'dashboard', name: 'Dashboard', iconUrl:'assets/images/dashboard_white.png',iconUrlActive:'assets/images/dashboard_color.png', type: 'link'},
  //   { state: 'search', name: 'Search', iconUrl:'assets/images/search_white.png',iconUrlActive:'assets/images/search_color.png', type: 'link'},
  //   { state: 'message', name: 'Message', iconUrl:'assets/images/message_white.png',iconUrlActive:'assets/images/message_color.png', type: 'link'},
  //   { state: 'match', name: 'My Matches', iconUrl:'assets/images/match_white.png',iconUrlActive:'assets/images/match_color.png', type: 'link'},
  //   { state: 'profile', name: 'My Profile', iconUrl:'assets/images/profile_white.png',iconUrlActive:'assets/images/profile_color.png', type: 'link'},
  //   { state: 'subscription', name: 'Subscription', iconUrl:'assets/images/sub_white.png',iconUrlActive:'assets/images/sub_color.png', type: 'link'},
  //   { state: 'payment', name: 'Payment', iconUrl:'assets/images/payment_white.png', type: 'link',iconUrlActive:'assets/images/payment_color.png'},
  //   { state: 'favorite', name: 'My Favorites', iconUrl:'assets/images/fav_white.png',iconUrlActive:'assets/images/fav_color.png', type: 'link'},
  //   { state: 'blog', name: 'My Blogs', iconUrl:'assets/images/fav_white.png',iconUrlActive:'assets/images/fav_color.png', type: 'link'},
  //   { state: 'Other Profile', name: 'Other Profile', iconUrl:'assets/images/fav_white.png',iconUrlActive:'assets/images/fav_color.png', type: 'link'},

  //      ];
  //  MENUACCOUNT = [
  //       { state: 'setting', name: 'Settings', iconUrl:'assets/images/setting_white.png',iconUrlActive:'assets/images/setting_color.png'},
  //       { state: 'term', name: 'Terms & Condition', iconUrl:'assets/images/term_white.png',iconUrlActive:'assets/images/term_color.png'},
  //          ];

  constructor(
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    public menuItems: MenuItems,
    public common :CommonService,
    public router :Router
  ) {
    this.mobileQuery = media.matchMedia('(min-width: 768px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
    console.log('mobileQuery',this.mobileQuery.matches)
  }

  ngOnInit() {
  
    $(".rotate").click(function () {
      $(this).toggleClass("down");
  })
  this.currentScreenWidth = screen.width ;
  if( this.currentScreenWidth <=  780) {
    this.isMobile = true;
  }
  console.log(this.currentScreenWidth,"CURRENTSCREEN")
    // this.streamService.getSubscription().subscribe((data: string) => {
    //   this.message = data;
      
    //   if (data) {
    //     setTimeout(() => {
    //       this.streamService.streamMessage('');
    //     }, 5000);
    //   }
    // });
  }

  navigateSroll() {
    this.showScroll = this.NoScroll.indexOf(this.headerTitle) == -1;
  }


  WindowResize(e) {
    // setTimeout(() => { window.dispatchEvent(new Event('resize')); }, 260);
    }

    ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  readLocalStorageValue(key: string): number {
    return parseInt(localStorage.getItem(key));
  }
  logOut() {
    this.common.logOut();
  }
  ngAfterViewInit() {}

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.screenWidth = window.innerWidth;
    this.screenHeight = window.innerHeight;
    console.log(this.screenWidth,'sss')
    if( this.screenWidth <= 780) {
      console.log("ISMOBILE TRUE")
      this.isMobile = true;
      
    }
    else {
      console.log("ISMOBILE FALSEs")
      this.isMobile =false;
    }

     console.log('isMobile',this.isMobile)
  }
}
