import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { NetworkService } from '../shared/network.service';
@Injectable({
  providedIn: 'root'
})
export class AuthServiceLocal {
  private pageChange = new BehaviorSubject('false');
  currentPage = this.pageChange.asObservable();

  constructor(private networkService: NetworkService,
    ) { }

  Page(message: string) {
    this.pageChange.next(message)
  }
  validateLogin(req: any) {
    return this.networkService.login("api/user/login", req, null, null);
  }
  addCard(req: any) {
    return this.networkService.post("api/card", req, null,"bearer");
  }
  signUpApi(req: any) {
    return this.networkService.login("api/user", req, null, null);
  }
  addSubscription(req: any) {
    return this.networkService.post("api/subscription", req, null,"bearer");
  }
  getSubscription() {
    return this.networkService.get("api/subscription", null, null,"bearer");
  }
 
  updateUser(req: any) {
    return this.networkService.put("api/user", req, null, "bearer");
  }
  validateFBLogin(req: any) {
    return this.networkService.login("api/user/social/login", req, null, null);
  }
  
  forgotPassword(req: any) {
    return this.networkService.post("api/user/password/forgot", req, null, null);
  }
  resetpassword(req: any) {
    return this.networkService.post("api/user/verify/password/forgot", req, null, null);
  }

  createOTP(request:any) {
    return this.networkService.post("api/otp/create", request, null,"bearer");
  }
  verifyOTP(request:any) {
    return this.networkService.post("api/otp/verify", request, null,"bearer");
  }
  uploadImage(image: any) {
    const formData = new FormData();

    formData.append("image", image, 'image.jpg');
    return this.networkService.uploadImages("api/s3upload/image-upload", formData, null, "bearer");
  }
  uploadImage1(image: any) {
    const formData = new FormData();
    formData.append("image", image, 'image.jpg');
    return this.networkService.post("api/s3upload/image-upload", formData, { observe: 'response' });
  }



  socialLogin(dataToPost: any){
    return this.networkService.post("api/users", dataToPost, null,null);
  }

  getCategory() {
    return this.networkService.get("api/category", null, null,null);
  }
  getUser() {
    return this.networkService.get("api/user/userList", null, null,"bearer");
  }
  verifyUser(req:any) {
    return this.networkService.post("api/user/verify", req, null,null);
  }
  ConnectUser (body:any) {
    return this.networkService.post("api/connect", body, null,"bearer");
  }
  AddFavourite(request:any) {
    return this.networkService.post("api/favorite", request, null,"bearer");
  }
}
